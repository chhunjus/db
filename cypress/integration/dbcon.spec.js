describe('db', () => {
    
it('connect', ()=>{
    const query = {
        drop_database: `DROP DATABASE IF EXISTS np_orms_db WITH (FORCE);`,
        create_database: `CREATE DATABASE np_orms_db;`,
    }
    Object.values(query).map(value => cy.task("Query", value))
    cy.task("dbQuery", `CREATE extension citext;`)
    // cy.exec(`pg_dump 'host=localhost port=5432 user=postgres password=postgres dbname=np_orms_db' < cypress/fixtures/dumping`, {failOnNonZeroExit: false})
    cy.task("lsWithGrep")
})


})
