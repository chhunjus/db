/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

/**
 * @type {Cypress.PluginConfig}
 */
// eslint-disable-next-line no-unused-vars
module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
  on("task", {
      Query: Query,
      dbQuery: dbQuery,
      lsWithGrep: lsWithGrep
  });
  return config;
};

const { Pool } = require('pg');
const dbConfig = require('../../cypress/fixtures/dbConfig.json');
const dataConfig = require('../../cypress/fixtures/dataConfig.json');

function Query(query) {
  const pool = new Pool(dbConfig.db)
  return new Promise((resolve, reject) => {
      pool.connect(function(err) {
          if (err) throw err;
          console.log("connected to db")
          pool.query(query, (err, res) => {
              pool.end()
              if (err) {
                  reject(err)
              } else {
                  resolve(res)
              }
          })
      })
  })
}

function dbQuery(query) {
  const pool = new Pool(dataConfig.db)
  return new Promise((resolve, reject) => {
      pool.connect(function(err) {
          if (err) throw err;
          console.log("connected to db")
          pool.query(query, (err, res) => {
              pool.end()
              if (err) {
                  reject(err)
              } else {
                  resolve(res)
              }
          })
      })
  })
}


    const util = require('util');
    const exec = util.promisify(require('child_process').exec);
    async function lsWithGrep() {
  try {
      const { stdout, stderr } = await exec(`pg_restore -d "host=localhost port=5432 dbname=np_orms_db user=postgres password=postgres" < C:/Users/chhun/OneDrive/Desktop/db/cypress/fixtures/dumping`);
      console.log('stdout:', stdout);
      console.log('stderr:', stderr);
      return true
  }catch (err) {
     console.error(err);
     return false
  };
    };



