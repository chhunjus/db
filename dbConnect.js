const { Client } = require('pg');

const config = new Client({
    user: 'postgres',
    password: 'postgres',
    port: 5432,
    host: 'localhost'
  })

  const configa = new Client({
    user: 'postgres',
    password: 'postgres',
    port: 5432,
    host: 'localhost',
    database: 'hello'
  })


  const execute = async (query) => {
    try {
        await config.connect();     // gets connection
        await config.query(query);  // sends queries
        return true;
    } catch (error) {
        console.error(error.stack);
        return false;
    } finally {
        await config.end();
        console.log('connection closed')         // closes connection
    }
};

const createDatabase = async () => {
    try {
        console.log('Connecting')
        await config.connect();                            // gets connection
        await config.query('CREATE DATABASE my_database'); // sends queries
        return true;
    } catch (error) {
        console.error(error.stack);
        return false;
    } finally {
        await config.end();                                // closes connection
    }
};

execute('DROP DATABASE IF EXISTS my_database WITH (FORCE)').then(result => {
    if (result) {
        console.log(result)
        console.log('Database removed');
    }

}).then(()=>{
    createDatabase().then((result) => {
        if (result) {
            console.log('Database created');
        }
    });
})
